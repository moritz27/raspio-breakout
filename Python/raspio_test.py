import RPi.GPIO as GPIO #install: sudo apt-get install python3-rpi.gpio
from time import sleep

shutdown_button_pin = 16
user_button_pin = 17

led_pin = 13
led_state = 0

pwm_pin = 18
pwm_state = 0

external_pin = 26
external_state = 0


def user_button_callback(channel):
    print("user_button_pin was pushed!")

def shutdown_button_callback(channel):
    print("shutdown_button_pin was pushed!")

    
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BCM) # Use physical pin numbering
GPIO.setup(user_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(shutdown_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(led_pin, GPIO.OUT, initial=0)
GPIO.setup(pwm_pin, GPIO.OUT, initial=0)
GPIO.setup(external_pin, GPIO.OUT, initial=0)  
    
GPIO.add_event_detect(user_button_pin,GPIO.FALLING,callback=user_button_callback)
GPIO.add_event_detect(shutdown_button_pin,GPIO.FALLING,callback=shutdown_button_callback)


while True: # Run forever
    print("High")
    GPIO.output(pwm_pin, GPIO.HIGH) # Turn on
    sleep(1) # Sleep for 1 second
    print("Low")
    GPIO.output(pwm_pin, GPIO.LOW) # Turn off
    sleep(1) # Sleep for 1 second


GPIO.cleanup() # Clean up
    
message = input("Press enter to quit\n\n") # Run until someone presses enter





#! /usr/bin/env python
import os
import RPi.GPIO as GPIO #install: sudo apt-get install python3-rpi.gpio
import time

sleepTime1=0.5
sleepTime2=0.1

GPIO.setmode(GPIO.BCM)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP) #Button
GPIO.setup(13, GPIO.OUT) #LED

try:
    for i in range (0,5):
        GPIO.output(24, GPIO.HIGH)
        time.sleep(sleepTime1)
        GPIO.output(24, GPIO.LOW)
        time.sleep(sleepTime1)

    while True:
        GPIO.output(24, GPIO.HIGH)
        GPIO.wait_for_edge(21, GPIO.FALLING)
        for i in range (0,5):
            GPIO.output(24, GPIO.HIGH)
            time.sleep(sleepTime1)
            GPIO.output(24, GPIO.LOW)
            time.sleep(sleepTime1)
        GPIO.output(24, GPIO.HIGH)
        # shut down the rpi
        os.system("/sbin/shutdown -h now")
except:
    GPIO.cleanup()
